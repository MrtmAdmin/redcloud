// Karma configuration file, see link for more information
// https://karma-runner.github.io/0.13/config/configuration-file.html

module.exports = function (config) {
    config.set({
        basePath: '',
        frameworks: ['jasmine', '@angular/cli'],
        plugins: [
            require('karma-jasmine'),
            require('karma-chrome-launcher'),
            require('karma-jasmine-html-reporter'),
            require('karma-coverage-istanbul-reporter'),
            require('@angular/cli/plugins/karma')
        ],
        client: {
            clearContext: false // leave Jasmine Spec Runner output visible in browser
        },
        files: [
            {pattern: './src/test.ts', watched: false},
            {pattern: './src/assets/css/*.css', included: true},
            {pattern: './src/assets/fonts/*.css', included: true},
            {pattern: './src/assets/images/*.png', included: true},
            "./node_modules/bootstrap/dist/css/bootstrap.min.css",
            "./node_modules/primeng/resources/themes/omega/theme.css",
            "./node_modules/primeng/resources/primeng.min.css",
            "./src/styles.css",
            "./node_modules/jquery/dist/jquery.min.js",
            "./src/assets/js/script.js"
        ],
        preprocessors: {
            './src/test.ts': ['@angular/cli']
        },
        mime: {
            'text/x-typescript': ['ts', 'tsx']
        },
        coverageIstanbulReporter: {
            reports: ['html', 'lcovonly'],
            fixWebpackSourcePaths: true
        },
        angularCli: {
            config: './angular-cli.json',
            environment: 'dev'
        },
        reporters: config.angularCli && config.angularCli.codeCoverage
                ? ['progress', 'coverage-istanbul']
                : ['progress', 'kjhtml'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['Chrome'],
        singleRun: false,
        browserNoActivityTimeout: 100000
    });
};
