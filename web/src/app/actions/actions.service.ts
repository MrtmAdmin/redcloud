/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * Service: This service is used to handle suppressions
 * @Author: Shreya Shah
 */

import {Injectable} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {Configuration} from '../services/configuration';
import {HttpService} from '../services/http.service';

@Injectable()
export class ActionsService {

    private actionUrl: string;
    constructor(private http: HttpService, private configuration: Configuration) {
        this.actionUrl = configuration.ServerWithApiUrl;
    }
}