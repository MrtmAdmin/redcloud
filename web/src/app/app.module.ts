import * as $ from 'jquery';

import {BrowserModule} from '@angular/platform-browser';
import {NoopAnimationsModule, BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule, BaseRequestOptions, XHRBackend} from '@angular/http';
import './rxjs-extensions';

import {AppRoutingModule, routedComponents} from './app-routing.module';
import {AppComponent} from './app.component';
import {FooterComponent} from './footers/footer.component';
import {AuthGuard} from './guards/auth.guard';
import {ResetPasswordGuard} from './guards/resetpassword.guard';
import {AuthService} from './login/authentication.service';
import {LoginComponent} from './login/login.component';
import {EventDetectorModule} from './eventdetector/eventdetector.module';
import {SuppressionModule} from './suppression/suppression.module';
import {UsecaseModule} from './usecases/usecases.module';
import {DeploymentRequestModule} from './deploymentrequest/deploymentrequest.module';
import {FeedsModule} from './feeds/feeds.module';
import {ConsumerModule} from './consumer/consumer.module';
import {CustomerContextRecordsModule} from './customercontextrecords/customer-context-records.module';
import {ActionsModule} from './actions/actions.module';
import {SegmentModule} from './segments/segment.module';
import {UserManagementModule} from './usermanagement/usermanagement.module';
import {Configuration} from './services/configuration';
import {ConfirmDialogModule, ConfirmationService} from 'primeng/primeng';
import {DialogModule} from 'primeng/primeng';
import {HeaderModule} from './headers/header.module';
import {Common} from './common/common';
import {Constant} from './common/constant';
import {ForgotPasswordComponent} from './forgotpassword/forgotpassword.component';
import {ResetPasswordComponent} from './resetpassword/resetpassword.component';
import {ChangePasswordComponent} from './changepassword//changepassword.component';
import {EqualValidator} from './validators/equal-validator.directive';  // import validator
import {ErrorHandlerComponent} from './errorhandler/error-handler.component';
import {ErrorHandler} from './errorhandler/error-handler';
import {NgIdleModule} from '@ng-idle/core';
import {HttpService} from './services/http.service';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {UserManagementComponent} from './usermanagement/usermanagement.component';
import {ComingSoonComponent} from './comingsoon/comingsoon.component';
import {ExplorationComponent} from './exploration/exploration.component';
import {MonitoringComponent} from './monitoring/monitoring.component';
import {ToasterService} from './services/toaster.service';
import {GrowlModule} from 'primeng/primeng';
import {SafePipe} from '../app/pipes/safeurl.pipe';
// rcw:added | Due to angular 2 Error: httpFactory lambada exported before uses
export function httpFactory(backend, options) {
    return new HttpService(backend, options);
}

@NgModule({
    imports: [
        BrowserModule,
        NoopAnimationsModule,
        BrowserAnimationsModule,
        FormsModule,
        AppRoutingModule,
        SuppressionModule,
        HttpModule,
        EventDetectorModule,
        UsecaseModule,
        DeploymentRequestModule,
        FeedsModule,
        ConsumerModule,
        CustomerContextRecordsModule,
        ActionsModule,
        SegmentModule,
        UserManagementModule,
        ConfirmDialogModule,
        HeaderModule,
        DialogModule,
        GrowlModule,
        NgIdleModule.forRoot()
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        FooterComponent,
        routedComponents,
        ForgotPasswordComponent,
        ResetPasswordComponent,
        ChangePasswordComponent,
        ErrorHandlerComponent,
        ComingSoonComponent,
        EqualValidator,
        ExplorationComponent,
        MonitoringComponent,
        SafePipe
    ],
    exports: [
        HeaderModule
    ],
    providers: [
        AuthGuard,
        ResetPasswordGuard,
        AuthService,
        Configuration,
        ConfirmationService,
        ErrorHandler,
        Common,
        UserManagementComponent,
        ToasterService,

        Constant,
        {
            provide: HttpService,
            useFactory: httpFactory,
            deps: [XHRBackend, BaseRequestOptions]
        },
        BaseRequestOptions,
        {provide: LocationStrategy, useClass: HashLocationStrategy}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
