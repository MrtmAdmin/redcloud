/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import {Injectable} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {Configuration} from '../services/configuration';
import {HttpService} from '../services/http.service';
import {Common} from '../common/common';

@Injectable()
export class DeploymentRequestService {

    private actionUrl: string;
    constructor(private http: HttpService, private configuration: Configuration,private common:Common) {
        this.actionUrl = configuration.ServerWithApiUrl;
    }
}

