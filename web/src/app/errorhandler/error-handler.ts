import { Injectable } from "@angular/core";
import { Constant } from '../common/constant';

@Injectable()
export class ErrorHandler {

    public content: string;
    public header : string;
    public buttonName : string;
    public buttonLink : string

    constructor(private constant:Constant) { 
      this.content="No error currently";
      this.header="Error";
      this.buttonName="Go To Dashboard";
      this.buttonLink=this.constant.dashboardUrl;
    }
}