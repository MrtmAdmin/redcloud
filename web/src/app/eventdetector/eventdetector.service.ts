import {Injectable} from '@angular/core';
import {HttpService} from '../services/http.service';
import 'rxjs/add/operator/toPromise';
import {EventDetector} from '../eventdetector/eventdetector';
import {Configuration} from '../services/configuration';
import {Common} from '../common/common';

@Injectable()
export class EventDetectorService {
    
    private actionUrl: string;
    constructor(private http: HttpService,private configuration: Configuration,private common:Common) {
        this.actionUrl = configuration.ServerWithApiUrl;
    }

    get(): Promise<EventDetector[]> {
        return this.http
            .get(this.actionUrl +'ered/list/eventdetectors')
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }
    
    getPaged(pageIndex:number,pageSize:number): Promise<EventDetector[]> {
        return this.http
            .get(this.actionUrl + 'ered/page/eventdetectors?pageIndex=' + pageIndex + '&pageSize=' + pageSize)
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }

    getById(id: number): Promise<EventDetector> {
        return this.http.get(this.actionUrl +"ered/eventdetectors/" + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }

    add(eventDetector: EventDetector) : Promise<any> {
        return this.http.post(this.actionUrl +"ered/eventdetectors", eventDetector)
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }
    
    update(eventDetector: EventDetector) : Promise<any> {
        return this.http.put(this.actionUrl +"ered/eventdetectors/", eventDetector)
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }
    
    publish(eventDetector: EventDetector) : Promise<any> {
        return this.http.post(this.actionUrl +"ered/publish/eventdetectors/", eventDetector)
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }
    
    copy(id: number): Promise<any> {
        console.log("id :"+id)
        return this.http.put(this.actionUrl +"ered/copy/eventdetectors/"+id,"")
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }
    
    archive(id: number): Promise<any> {
        return this.http.put(this.actionUrl +"ered/archive/eventdetectors/"+id,"")
            .toPromise()
            .then(response => response)
            .catch(this.common.handleError);
    }
    
    delete(id: number): Promise<any> {
        return this.http.delete(this.actionUrl +"ered/eventdetectors/"+id)
            .toPromise()
            .then(response => response)
            .catch(this.common.handleError);
    }
    nextVersion(id: number): Promise<any> {
        return this.http.put(this.actionUrl +"ered/version/eventdetectors/"+id, "")
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }
}