/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import {Injectable} from '@angular/core';
import {HttpService} from '../services/http.service';
import 'rxjs/add/operator/toPromise';
import {Feeds} from '../feeds/feeds';
import {Configuration} from '../services/configuration';
import {Common} from '../common/common';

@Injectable()
export class FeedsService {
    
    private actionUrl: string;
    constructor(private http: HttpService,private configuration: Configuration,private common:Common) {
        this.actionUrl = configuration.ServerWithApiUrl;
    }
    
    get(): Promise<Feeds[]> {
        return this.http
            .get(this.actionUrl +'ered/feeds')
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }
}
