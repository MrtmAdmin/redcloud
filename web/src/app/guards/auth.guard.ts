import { Injectable } from '@angular/core';
import { Router, CanActivate, CanDeactivate } from '@angular/router';
import { ConfirmationService } from 'primeng/primeng';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs';
import { AuthService } from '../login/authentication.service';
import { User } from '../usermanagement/user';
import { Common } from '../common/common';
import { Constant } from '../common/constant';
import { HeaderService } from '../headers/header.service';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import * as _ from 'lodash';
import { ErrorHandler } from '../errorhandler/error-handler';

@Injectable()
export class AuthGuard implements CanActivate, CanDeactivate<any> {

    loggedInUserObj = new User();
    mainMenu: any = {};
    mainMenuRouterLinkMap: any = {};

    constructor(private router: Router,
        private authService: AuthService,
        private confirmationService: ConfirmationService,
        private common: Common,
        private headerService: HeaderService,
        private errorHandler: ErrorHandler,
        private constant: Constant
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        let isLoggedIn = this.common.isLoggedIn();
        this.loggedInUserObj = this.common.getLoggedInUser();
        this.mainMenu = this.headerService.getMenu();
        this.mainMenuRouterLinkMap = this.headerService.getMainMenu();
        let currentlink = state.url;
        if (isLoggedIn && this.common.getSessionCookie()) {

            if (this.constant.defaultAccessRights.indexOf(currentlink) > -1) {
                return true;
            }
            //If the current feature has no access in both submenu and mainmenu - return false
            if (!this.hasAccessSubMenu(currentlink) && !this.hasAccessMainMenu(currentlink)) {
                this.errorHandler.content = "You are not authorised to access this page.";
                this.errorHandler.header = "Access Denied!";
                this.errorHandler.buttonName = "Go to Dashboard";
                this.errorHandler.buttonLink = this.constant.dashboardUrl;
                this.router.navigate([this.constant.errorUrl]);
                return false;
            }
            return true;
        } else if (isLoggedIn && !this.common.getSessionCookie()) {
            this.authService.logout(false);
        } else {
            if (currentlink == '/forgotpassword') {
                return true;
            } else {
                // not logged in so redirect to login page
                this.router.navigate([this.constant.loginUrl]);
                return false;
            }
        }
    }

    canDeactivate(target: any) {
        let ctrl = this;
        let isLoggedIn = this.common.isLoggedIn();
        if (target.hasChanges() && isLoggedIn) {
            return Observable.create((observer: Observer<boolean>) => {
                this.confirmationService.confirm({
                    message: 'You have unsaved changes. Are you sure you want to leave this page?',
                    accept: () => {
                        observer.next(true);
                        observer.complete();
                    },
                    reject: () => {
                        observer.next(false);
                        observer.complete();
                    }
                });
            });
        } else {
            return true;
        }
    }

    // rcw:comment Shreya |  - To check the access of Link for the loggedin user from submenu features
    hasAccessSubMenu(link: string) {
        let valueoflink: string;
        let ctrl = this;
        let returnAccessValue: boolean = false;
        /*mainMenu is mapped as Dashboard as some submenu and so on
        *so using .mapValues to fetch the array of features in each main menu
        *and then once we have a array of features of a main menu 
        *using .find - searching if the currentlink is there 
        */
        _.mapValues(this.mainMenu, function (features) {
            let neededFeature = _.find(features, function (feature) {
                return feature.routerLink === decodeURI(link);
            });
            if (neededFeature !== undefined) {
                //After finding - getting the value (name) of link e.g. /eventdetectors has value EVENT_DETECTOR
                valueoflink = neededFeature.value;
                let access = "";
                if (ctrl.loggedInUserObj.screenPermissions != undefined && ctrl.loggedInUserObj.screenPermissions != null && ctrl.loggedInUserObj.screenPermissions.length > 0) {
                    for (let item of ctrl.loggedInUserObj.screenPermissions) {
                        if (item.screenName == valueoflink) {
                            access = item.accessLevel;
                        }
                    }
                }
                //If permission is READ_ONLY or UPDATE_ACCESS , then only it will return true
                if (access == ctrl.constant.READ_ONLY || access === ctrl.constant.UPDATE_ACCESS) {
                    returnAccessValue = true;
                }
            }
        });
        return returnAccessValue;
    }

    // rcw:comment Shreya |  - To check the access of Link for the loggedin user from mainmenu features
    hasAccessMainMenu(routerLink: string) {
        //Using .findKey to get the name of link (feature) based on link. i.e. getting the key based on value
        let feature = _.findKey(this.mainMenuRouterLinkMap, function (accessLink) { return accessLink === routerLink });
        let access = "";
        if (this.loggedInUserObj.screenPermissions != undefined && this.loggedInUserObj.screenPermissions != null && this.loggedInUserObj.screenPermissions.length > 0) {
            for (let item of this.loggedInUserObj.screenPermissions) {
                if (item.screenName == feature) {
                    access = item.accessLevel;
                }
            }
        }
        //If permission is READ_ONLY or UPDATE_ACCESS , then only it will return truel
        if (access === this.constant.READ_ONLY || access === this.constant.UPDATE_ACCESS) {
            return true;
        }
        return false;
    }
}
