import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs';
import { ErrorHandler } from '../errorhandler/error-handler';
import { UserManagementService } from '../usermanagement/usermanagement.service';
import { AuthService } from '../login/authentication.service';
import { Constant } from '../common/constant';

@Injectable()
export class ResetPasswordGuard implements CanActivate {

    isRequesting: boolean = false;

    constructor(
        private router: Router,
        private userManagementService: UserManagementService,
        private errorHandler: ErrorHandler,
        private authService: AuthService,
        private constant: Constant
    ) { }

    /* rcw:comment Shreya | - This method identifies the Url request for resetting
        the password before going to the reset passwrod page.
        It checks whther the request is for forgot password or for setting password for the new user.
        If it is for the new user then only email needs to be checked 
        while if it is the existing user then auth token as well as username needs to be checked.*/
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        let ctrl = this;
        if (route.queryParams['verification_token'] && route.queryParams['username']) {

            return Observable.create((observer: Observer<boolean>) => {
                this.isRequesting = true;
                this.authService.verifyToken(route.queryParams['username'], route.queryParams['verification_token']).then(function () {
                    observer.next(true);
                    observer.complete();
                }).catch(function (error) {
                    if (error && error._body) {
                        ctrl.errorHandler.content = JSON.parse(error._body).error_description;
                        ctrl.errorHandler.buttonName = "Login";
                        ctrl.errorHandler.buttonLink = ctrl.constant.loginUrl;
                        ctrl.router.navigate(['/error']);
                        observer.next(false);
                        observer.complete();
                    }
                    ctrl.isRequesting = false;
                });
            });
        } else {
            ctrl.router.navigate(['/login']);
        }
    }
}
