import {Component, OnInit, AfterViewInit} from '@angular/core';
import {AuthService} from '../login/authentication.service';
import {User} from '../usermanagement/user';
import {Common} from '../common/common';
import {Constant} from '../common/constant';
import * as _ from 'lodash';
import {HeaderService} from './header.service';
import {Router} from '@angular/router';
import {UserManagementComponent} from '../usermanagement/usermanagement.component';
import {UserManagementService} from '../usermanagement/usermanagement.service';
import {ConfirmDialogModule, ConfirmationService} from 'primeng/primeng';
@Component({
    moduleId: module.id,
    selector: 'header',
    templateUrl: 'header.component.html',
})
export class HeaderComponent implements OnInit, AfterViewInit {

    selectValue: string = 'en';
    loggedInUserObj = new User();
    mainMenu: any = {};
    mainMenuRouterLinkMap: any = {};
    profileImageUrl: any = null;
    popUp: boolean = false;
    isRequesting = false;
    subscription: any = null;
    InvalidImageDisplay: boolean = false;
    constructor(
        private router: Router,
        private authService: AuthService,
        private common: Common,
        private constant: Constant,
        private headerService: HeaderService,
        private usermanagement: UserManagementComponent,
        private userManagementService: UserManagementService, ) {
    }

    ngOnInit(): void {
        this.mainMenu = this.headerService.getMenu();
        this.mainMenuRouterLinkMap = this.headerService.getMainMenu();
        this.loggedInUserObj = this.common.getLoggedInUser();
        //this.getBase64(this.loggedInUserObj.userProfile);
        let ctrl = this;
        this.profileImageUrl = this.common.getLoggedInProfilePic();
        if (this.profileImageUrl == "") {
            this.profileImageUrl = null;
        }
        this.subscription = this.common.getProfilePicValue()
            .subscribe(function (item: any) {
                ctrl.profileImageUrl = ctrl.getBase64(item);
            });
    }

    ngAfterViewInit() {
        let ctrl = this;
        //Submenu hover
        $('.subhoverLink').click(function (e) {
            $('.outer_submenu').stop();
            if ($($(this).data('id')).is(":hidden")) {
                $('.navsub').hide();
                $('.subhoverLink').removeClass('subhoverLinkTringle');
                $(this).addClass('subhoverLinkTringle');
                $($(this).data('id')).show().css('padding-left', $(this).position().left + 9);
                $('.outer_submenu').slideDown();
            } else {
                $('.subhoverLink').removeClass('subhoverLinkTringle');
                $('.outer_submenu').slideUp();
            }
        });
        $(document).click(function (e) {
            if (!$(e.target).is('.subhoverLink')) {
                $('.subhoverLink').removeClass('subhoverLinkTringle');
                $('.outer_submenu').slideUp();

            }
        });


        setTimeout(() => {
            this.selectValue = localStorage.getItem('localeId');
        }, 1);
        $('.clickPopUp').click(function (e) {
            $('.subhoverLink').removeClass('subhoverLinkTringle');
            $('.outer_submenu').slideUp();
            if (ctrl.popUp) {
                ctrl.popUp = false;
            }
            else {
                ctrl.popUp = true;
            }
            e.stopPropagation();
        })
        $('#headerPopup').click(function (e) {
            ctrl.popUp = true;
            e.stopPropagation();
        })

        $(window).click(function () {
            ctrl.popUp = false;

        })
    }
    displayMainMenu(key) {
        let hasFeatures = this.generateMenuItem(key);
        if (!hasFeatures) {
            return this.checkFeatureAccess(key);
        }
        return true;
    }
    popUpFun(e) {
        this.popUp = true;
        e.stopPropagation();
    }
    openPopUp($event) {
        this.popUp = false;
    }
    viewProfile() {
        this.popUp = false;
        this.common.change();
        this.common.viewProfileDisplay = true;
        this.router.navigateByUrl('/users');
    }
    generateMenuItem(key) {
        let features = this.mainMenu[key];
        if (features != undefined && features != null) {
            if (this.loggedInUserObj != null && this.loggedInUserObj.screenPermissions != undefined && this.loggedInUserObj.screenPermissions != null && this.loggedInUserObj.screenPermissions.length > 0) {
                for (let item of features) {
                    for (let permission of this.loggedInUserObj.screenPermissions) {
                        if (permission.screenName == item.value && this.checkFeatureAccess(item.value)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    // rcw:comment Shreya | - Convert Image FILE to base64 URL
    getBase64(file) {
        if (file != undefined && file != null) {
            let ctrl = this;
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                ctrl.profileImageUrl = reader.result;
            };
            reader.onerror = function (error) {
                console.log('Error: ', error);
            };
        }
    }

    public checkFeatureAccess(name: string) {
        if (this.loggedInUserObj != null && this.loggedInUserObj.screenPermissions != undefined && this.loggedInUserObj.screenPermissions != null && this.loggedInUserObj.screenPermissions[name] != undefined && this.loggedInUserObj.screenPermissions[name] !== null) {
            let access = "";
            for (let item of this.loggedInUserObj.screenPermissions) {
                if (item.screenName == name) {
                    access = item.accessLevel;
                }
            }
            if (access == this.constant.READ_ONLY || access == this.constant.UPDATE_ACCESS) {
                return true;
            }
        }
        return false;
    }
    public onChange(localeId: string): void {
        localStorage.setItem('localeId', localeId);
        location.reload(false);
    }

    public logout() {
        this.popUp = false;
        this.router.navigate(['/login'], {queryParams: {action: "logout"}});
    }
    invalidImageDisplay() {
        this.InvalidImageDisplay = false;
    }
    onSelect(event: any) {
        let ctrl = this;
        //checks whether the uploaded image size is valid or not
        if (event.files[0].size > 1000000) {
            this.InvalidImageDisplay = true;
            this.common.changeImageError();
        }
        //checks whether the uploaded file is valid or not
        else {
            var file = event.files[0];
            var fileType = file["type"];
            var ValidImageTypes = ["image/gif", "image/jpeg", "image/png", "image/jpg", "image/ico"];
            if ($.inArray(fileType, ValidImageTypes) < 0) {
                this.common.changeFileInvalid();
            }
            else {
                this.isRequesting = true;
                this.usermanagement.uploadedFiles = [];
                this.usermanagement.isProfilePictureChanged = true;
                this.usermanagement.uploadedFiles.push(event.files[0]);
                this.loggedInUserObj.userProfile = this.usermanagement.uploadedFiles[0];
                if (ctrl.loggedInUserObj.userProfile != null && ctrl.loggedInUserObj.userProfile != undefined) {
                    ctrl.userManagementService.uploadUserAvatar(ctrl.loggedInUserObj.userProfile, ctrl.loggedInUserObj.email)
                        .then(function () {
//                            localStorage.removeItem(ctrl.constant.userProfilePic);
                            ctrl.getBase64(ctrl.loggedInUserObj.userProfile);
                            ctrl.common.changeingProfilePicInHeader(ctrl.loggedInUserObj.userProfile);
                            ctrl.userManagementService.getUserAvatar(ctrl.loggedInUserObj.email)
                                .then(function (profileImageUrl) {
                                    ctrl.isRequesting = false;
                                    if (profileImageUrl != "" && profileImageUrl !== null) {
                                        ctrl.profileImageUrl = "data:image/jpeg;base64," + profileImageUrl;
                                        localStorage.setItem(ctrl.constant.userProfilePic, ctrl.common.encrytStrng(ctrl.profileImageUrl));
                                    }
                                    else {
                                        ctrl.profileImageUrl = null;
                                    }
                                    ctrl.popUp = false;
                                })
                                .catch(function (error) {
                                    console.log("error", error);
                                });
                        });
                }
            }
        }
    }
    showAdmin(featureValue: string) {
        if (featureValue == 'USER_MANAGEMENT') {
            this.common.viewProfileDisplay = false;
            this.common.changeToUserManagementPage();
        }
    }
}
