/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import {Injectable} from '@angular/core';

@Injectable()
export class HeaderService {

    private mainMenu: any;
    private mainMenuRouterLinkMap;
    constructor() {
        this.mainMenu = {};
        this.mainMenuRouterLinkMap = {};
    }

    getMenu() {
        let menuItem = [];
        
          // rcw:comment Shreya | - Commenting for 6.0 release
//        menuItem.push({label: 'Executive', value: 'EXECUTIVE', routerLink: '', pageAccess:false,updateAccess:false});
//        menuItem.push({label: 'Campaign Analyst', value: 'CAMPAIGN_ANALYST', routerLink: '', pageAccess:false,updateAccess:false});
//        menuItem.push({label: 'Operation Analyst', value: 'OPERATION_ANALYST', routerLink: '', pageAccess:false,updateAccess:false});
//        this.mainMenu['DASHBOARD'] = menuItem;

//        menuItem = [];
//        menuItem.push({label: 'Event Detectors', value: 'EVENT_DETECTOR', routerLink: '/eventdetectors', pageAccess:false,updateAccess:false});
//        menuItem.push({label: 'Use Cases', value: 'USECASE', routerLink: '/usecases', pageAccess:false,updateAccess:false});
//        menuItem.push({label: 'Actions', value: 'ACTION', routerLink: '/actions', pageAccess:false,updateAccess:false});
//        menuItem.push({label: 'Offers', value: 'OFFER', routerLink: '', pageAccess:false,updateAccess:false});
//        menuItem.push({label: 'Workflows', value: 'WORKFLOW', routerLink: '', pageAccess:false,updateAccess:false});
//        menuItem.push({label: 'Segments', value: 'SEGMENT', routerLink: '/segments', pageAccess:false,updateAccess:false});
//        menuItem.push({label: 'Rewards', value: 'REWARD', routerLink: '', pageAccess:false,updateAccess:false});
//        menuItem.push({label: 'Communications', value: 'COMMUNICATION', routerLink: '', pageAccess:false,updateAccess:false});
//        menuItem.push({label: 'Models', value: 'MODEL', routerLink: '', pageAccess:false,updateAccess:false});
//        this.mainMenu['RULESETWORKSPACE'] = menuItem;

        menuItem = [];
        menuItem.push({label: 'Performance', value: 'PERFORMANCE', routerLink: '/performance/Performance', pageAccess: false, updateAccess: false});
        menuItem.push({label: 'Campaign Metrics', value: 'CAMPAIGN_METRICS', routerLink: '/campaignmetrics/Campaign Metrics', pageAccess: false, updateAccess: false});
        menuItem.push({label: 'BDNA', value: 'BDNA', routerLink: '/bdna/BDNA', pageAccess: false, updateAccess: false});
        menuItem.push({label: 'Drop-off & Offers', value: 'DROP_OFF_AND_OFFERS', routerLink: '/dropoffoffers/Drop-off And Offers', pageAccess: false, updateAccess: false});
        menuItem.push({label: 'Drop-off & Offers (Sim)', value: 'DROP_OFF_AND_OFFERS_SIM', routerLink: '/dropoffsimoffers/Drop-off And Offers (Sim)', pageAccess: false, updateAccess: false});
        this.mainMenu['EXPLORATION'] = menuItem;

//        menuItem = [];
//        this.mainMenu['MODELLING'] = menuItem;

//        menuItem = [];
//        this.mainMenu['CONNECTIONS'] = menuItem;
//        
        menuItem = [];
        menuItem.push({label: 'Overview', value: 'OVERVIEW', routerLink: '/overview/Overview', pageAccess: false, updateAccess: false});
        menuItem.push({label: 'Charts', value: 'CHARTS', routerLink: '/charts/Charts', pageAccess: false, updateAccess: false});
        menuItem.push({label: 'Environments', value: 'ENVIRONMENTS', routerLink: '/environments/Environments', pageAccess: false, updateAccess: false});
        menuItem.push({label: 'Alerts', value: 'ALERTS', routerLink: '/alerts/Alerts', pageAccess: false, updateAccess: false});
        this.mainMenu['MONITORING'] = menuItem;

//        menuItem = [];
//        menuItem.push({label: 'Deployment Request', value: 'DEPLOYMENT_REQUEST', routerLink: '/deploymentrequest', pageAccess:false,updateAccess:false});
//        menuItem.push({label: 'Deployment', value: 'OPERATIONS_DEPLOYMENT', routerLink: '/deployment', pageAccess:false,updateAccess:false});
//        menuItem.push({label: 'Pending Approval', value: 'PENDING_APPROVAL', routerLink: '/pendingapproval', pageAccess:false,updateAccess:false});
//        this.mainMenu['OPERATION'] = menuItem;

        menuItem = [];
        this.mainMenu['ADMIN'] = menuItem;
        menuItem.push({label: 'User Management', value: 'USER_MANAGEMENT', routerLink: '/users', pageAccess:false,updateAccess:false});

        return this.mainMenu;
    }

    getMainMenu() {
      //  this.mainMenuRouterLinkMap["ADMIN"] = '/users';
        return this.mainMenuRouterLinkMap;
    }
}

