import {TRANSLATIONS, TRANSLATIONS_FORMAT, LOCALE_ID} from '@angular/core';

declare var System: any;

interface TranslationFileContent {
    Translation: string;
}

export function getTranslationProviders(): Promise<Object[]> {

    // Set your locale here or get it from somewhere (e.g. localStorage)
    const locale = document['locale'] as string;

    const noProviders: Object[] = [];

    if (locale == 'en') {
        return Promise.resolve(noProviders);
    } else {
        return System.import(`../assets/locale/messages.${locale}.ts`)
            .then((translations: TranslationFileContent) => {
                return createProviders(translations, locale);
            })
            .catch(() => noProviders);
    }

}
function createProviders(translations: TranslationFileContent, locale: string) {
    return [
        {provide: TRANSLATIONS, useValue: translations.Translation},
        {provide: TRANSLATIONS_FORMAT, useValue: 'xlf'},
        {provide: LOCALE_ID, useValue: locale}
    ];
}