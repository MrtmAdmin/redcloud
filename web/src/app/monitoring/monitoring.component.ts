import {Component, AfterViewInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Common} from '../common/common';

@Component({
    selector: 'monitoring',
    templateUrl: './monitoring.component.html'
})

export class MonitoringComponent implements AfterViewInit {

    currentFeature: String = "Default";
    featureLink: String = "";

    constructor(
        private route: ActivatedRoute,
        private common: Common) {
        this.route.params.subscribe(params => {
            this.currentFeature = params['featureName'];
            if (this.currentFeature == "Overview") {
                this.featureLink = "http://192.168.1.186:55380/zabbix/dashboard.php?fullscreen=1";
            } else if (this.currentFeature == "Charts") {
                this.featureLink = "http://192.168.1.186:55380/zabbix/screens.php?ddreset=1&fullscreen=1";
            } else if (this.currentFeature == "Environments") {
                this.featureLink = "http://192.168.1.186:55380/zabbix/overview.php?application=&type=1&fullscreen=1";
            }else if (this.currentFeature == "Alerts") {
                this.featureLink = "http://192.168.1.186:55380/zabbix/events.php?ddreset=1&fullscreen=1";
            }
        });
    }

    ngAfterViewInit(): void {
        this.changeSizeFromViewPort();
    }

    changeSizeFromViewPort() {
        var panelGot = this.common.winResize('menuJ', null);
        $('.explore-frame').css('height', panelGot - 25);
    }

}



