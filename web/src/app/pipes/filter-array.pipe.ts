import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'filterarray'
})

export class FilterArrayPipe implements PipeTransform {
    transform(items: any, args: any, field: string): any {
        if(field == undefined || field == null || field == ''){
            field = 'name';
        }
        if (args == '' || args == undefined) {return items;}
        let query = args.toLowerCase();
        if (!items) return [];
        return items.filter(item => item[field].toLowerCase().indexOf(query) !== -1);
    }
}
