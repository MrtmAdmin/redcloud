/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {DataTableModule} from 'primeng/primeng';
import {DialogModule} from 'primeng/primeng';
import {GrowlModule} from 'primeng/primeng';
import {TooltipModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/primeng';
import {FileUploadModule} from 'primeng/primeng';
import {DndModule} from 'ngx-dnd';
import {NumbersOnly} from '../directives/numbers-only';
import {MultiselectDropdownModule} from '../directives/dropdown-multiselect/multiselect-dropdown';
import {QueryBuilderComponent} from '../directives/query-builder';
import {FilterArrayPipe} from '../pipes/filter-array.pipe';
import {MapToIterable} from '../pipes/map-to-Iterable.pipe';
import {CapitalizePipe} from '../pipes/capitalize.pipe';
import {ContextMenuModule} from '../directives/angular2-contextmenu/angular2-contextmenu';
import {ContextMenuService} from '../directives/angular2-contextmenu/src/contextMenu.service';
import {SpinnerComponent} from '../directives/spinner-component/spinner.component';
import {OnInitDirective} from '../directives/ng-oninit'

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        DataTableModule,
        DndModule.forRoot(),
        CalendarModule,
        TooltipModule,
        DialogModule,
        GrowlModule,
        ContextMenuModule,
        FileUploadModule,
        MultiselectDropdownModule
    ],
    exports: [
        CommonModule, 
        FormsModule,
        DataTableModule,
        DndModule,
        CalendarModule,
        TooltipModule,
        DialogModule,
        GrowlModule,
        FileUploadModule,
        MultiselectDropdownModule,
        FilterArrayPipe,
        MapToIterable,
        CapitalizePipe,
        NumbersOnly,
        QueryBuilderComponent,
        SpinnerComponent,
        ContextMenuModule,
        OnInitDirective
    ],
    declarations: [
        QueryBuilderComponent,
        NumbersOnly,
        FilterArrayPipe,
        MapToIterable,
        CapitalizePipe,
        SpinnerComponent,
        OnInitDirective
    ],
    providers: [ContextMenuService]
})
export class SharedModule {}
