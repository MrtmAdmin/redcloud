export class Suppression {
    public id: number;
    public suppressionId: number;
    public suppressionName: string;
    public suppressionDesc: string;
    public isInSimulation: boolean;
    public isInProduction: boolean;
    public updatable: boolean;
    public deletable: boolean;
    public archivable: boolean;
    public copyable: boolean;
    public versionable: boolean;
    public criteriaExpression: string;
    public version: number;
    public priority: number;
    public status: string;
    public environment: string;
    public failureLogLabel: string;
    public successLogLabel: string;
    public isEditable: boolean;
    public isNestedSuppressionRule: boolean;
    public owner: string;
    public lastUpdatedDate: string;
    public publishable:boolean;
    
    constructor() {
        this.suppressionId = null;
        this.status = "DRAFT";
        this.updatable = true;
        this.deletable = false;
        this.archivable = false;
        this.copyable = false;
        this.versionable = false;
        this.publishable = true;
        this.isInProduction = false;
        this.isInSimulation = false;
    };

}

export class SuppressionAsnBo {
    public suppressionId: number;
    public eventDetectorId: number;
    public orderOfExecution: number;
    public skipIndicator: number;

    constructor() {};
}