/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {UsecaseComponent} from './usecases.component';
import {UsecaseService} from './usecases.service';
import {SharedModule} from '../shared/shared.module';
import {AppRoutingModule} from '../app-routing.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        AppRoutingModule
    ],
    declarations: [
        UsecaseComponent
    ],
    providers: [UsecaseService]
})
export class UsecaseModule {

}

