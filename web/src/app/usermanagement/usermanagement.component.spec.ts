/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import {TestBed, async, ComponentFixture} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {BaseRequestOptions} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import {BrowserModule} from '@angular/platform-browser';
import {NoopAnimationsModule, BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {APP_BASE_HREF} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {Configuration} from '../services/configuration';
import {UserManagementComponent} from '../usermanagement/usermanagement.component';
import {UserManagementMockDataService} from '../usermanagement/usermanagement.mockdata.service';
import {UserManagementMockBackendProvider} from './usermanagement.mockbackend';
import {UserManagementService} from '../usermanagement/usermanagement.service';
import {Common} from '../common/common';
import {Constant} from '../common/constant';
import {HeaderService} from '../headers/header.service';
import * as _ from 'lodash';

describe('UserManagementComponent', () => {
    let userManagementCompInstance: UserManagementComponent;
    let fixture: ComponentFixture<UserManagementComponent>;
    let userManagementMockDataService = new UserManagementMockDataService();
    let common = new Common();

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                NoopAnimationsModule,
                BrowserAnimationsModule,
                SharedModule,
                BrowserModule
            ],
            declarations: [
                UserManagementComponent
            ],
            providers: [
                UserManagementService,
                HeaderService,
                Configuration,
                MockBackend,
                Common,
                Constant,
                UserManagementMockBackendProvider,
                BaseRequestOptions,
                {provide: APP_BASE_HREF, useValue: '/'}
            ]
        }).compileComponents();

    }));

    //rcw:comment Shreya | -To check whether the component exists or not
    it('should create the app', async(() => {
        console.log("Create User-Management Component")
        fixture = TestBed.createComponent(UserManagementComponent);
        userManagementCompInstance = fixture.debugElement.componentInstance;
        expect(userManagementCompInstance).toBeTruthy();
    }));

    // //rcw:comment Shreya | - Get logged in user from the storage
    it('get Logged-In User', async(() => {
        console.log("Get Logged In User")
        fixture = TestBed.createComponent(UserManagementComponent);
        userManagementCompInstance = fixture.debugElement.componentInstance;
        let userData = userManagementMockDataService.getUser(1);
        localStorage.setItem("user", common.encrytStrng(JSON.stringify(userData)));
        userManagementCompInstance.getLoggedInUser();
        expect(userManagementCompInstance.viewUserObj.userId).toBeDefined();
    }));

    //rcw:comment Shreya | -Show user details
    //     and calling the add function
    // from the component.
    it('show User Detail', function (done) {
        console.log("show User Detail");
        fixture = TestBed.createComponent(UserManagementComponent);
        userManagementCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            let userData = userManagementMockDataService.getUser(2);
            fixture.detectChanges();
            userManagementCompInstance.showUserDetail(userData, false, true);
            setTimeout(function () {
                fixture.detectChanges();
                expect(userManagementCompInstance.viewUserObj.userId).toBeDefined();
                done();
            }, 500);

        });
    });

    //rcw:comment Shreya | - Add the user
    it('Add User', function (done) {
        console.log("Add User");
        fixture = TestBed.createComponent(UserManagementComponent);
        userManagementCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            userManagementCompInstance.openAddUserPage();
            setTimeout(function () {
                fixture.detectChanges();
                userManagementCompInstance.userObj = userManagementMockDataService.createNewUser();
                fixture.detectChanges();
                userManagementCompInstance.addOrUpdateUsers();
                setTimeout(function () {
                    expect(userManagementCompInstance.userObj.userId).toBeNull(false);
                    done();
                }, 500);
                done();
            }, 500);
        });
    });

    //rcw:comment Shreya | - Update the user
    it('Update User', function (done) {
        console.log("Update User");
        fixture = TestBed.createComponent(UserManagementComponent);
        userManagementCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            let userData = userManagementMockDataService.getUser(2);
            fixture.detectChanges();
            userManagementCompInstance.showUserDetail(userData, false, true);
            setTimeout(function () {
                fixture.detectChanges();
                userManagementCompInstance.editUser();
                userManagementCompInstance.userObj.firstName = "Subhramaniyam"
                fixture.detectChanges();
                userManagementCompInstance.addOrUpdateUsers();
                setTimeout(function () {
                    fixture.detectChanges();
                    expect(userManagementCompInstance.userObj.firstName).toEqual('Subhramaniyam');
                    done();
                }, 500);
            }, 500);
        });
    });
    //rcw:comment Shreya | - Update the user
    it('Delete User', function (done) {
        console.log("Delete User");
        fixture = TestBed.createComponent(UserManagementComponent);
        userManagementCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            let userData = userManagementMockDataService.getUser(2);
            fixture.detectChanges();
            userManagementCompInstance.modalContent = userData;
            setTimeout(function () {
                fixture.detectChanges();
                userManagementCompInstance.deleteUser();
                setTimeout(function () {
                    fixture.detectChanges();
                    expect(userManagementCompInstance.viewUserObj.userId).not.toEqual(userData.userId);
                    done();
                }, 500);
            }, 500);
        });
    });
});


