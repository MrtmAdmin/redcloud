/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import * as $ from 'jquery';
import { Component, OnInit, AfterViewInit, ViewChild, OnChanges } from '@angular/core';
import { UserManagementService } from '../usermanagement/usermanagement.service';
import { Role } from '../usermanagement/role';
import { User } from '../usermanagement/user';
import { Common } from '../common/common';
import { Constant } from '../common/constant';
import * as _ from 'lodash';
import { ContextMenuService } from '../directives/angular2-contextmenu/src/contextMenu.service';
import { HeaderService } from '../headers/header.service';
import { ToasterService } from '../services/toaster.service';

@Component({
    selector: 'user-management',
    templateUrl: './usermanagement.component.html',
    styleUrls: ['./usermanagement.component.css']
})
export class UserManagementComponent implements OnInit, AfterViewInit {
    subscription: any;

    @ViewChild('userForm') form;

    @ViewChild('fileUpload') fileUpload;

    constructor(
        private common: Common,
        private contextMenuService: ContextMenuService,
        private userManagementService: UserManagementService,
        private constant: Constant,
        private headerService: HeaderService,
        private toasterService: ToasterService
    ) { };

    roleList: Role[] = [];
    featureScreenMap: any = {};
    userObj = new User();
    loggedInUser = new User();
    modalContent = new User();
    loggedInUserById = new User();
    loggedInUserRoles: string = '';
    viewUserInfo: boolean = true;
    editUserInfo: boolean = false;
    uploadedFiles: any[] = [];
    roleError: boolean = false;
    rolesSelected: boolean = false;
    viewUserObj = new User();
    loggedInUserDisplay: boolean = true;
    cancelUserDisplay: boolean = false;
    deleteUserDisplay: boolean = false;
    submittedFlag: boolean = false;
    profileImageUrl: any;
    currentProfilePicImageUrl: any = null;;
    isProfilePictureChanged: boolean = false;
    cancelChangeReason: string = '';
    isLoggedIn: boolean = false;
    isRequesting: boolean;
    configMenuOptions = [];
    updateAccess: boolean = false;
    featureAccessList: any[] = [];
    currentFeatureAccessList: any[] = [];
    mainMenu: any = {};
    currentMainMenu: any = {};
    readAccess: boolean = false;
    InvalidImageDisplay: boolean = false;
    InvalidFileDisplay: boolean = false
    viewProfileDisplay: boolean = this.common.viewProfileDisplay;

    ngOnInit(): void {
        this.updateAccess = this.common.checkUpdateAccess('USER_MANAGEMENT');
        this.readAccess = this.common.checkReadAccess('USER_MANAGEMENT');
        let ctrl = this;

        //User clicks on view profile from header dropdown - load /users
        this.subscription = this.common.getEmittedValue()
            .subscribe(item => {
                if (ctrl.form.dirty || ctrl.isProfilePictureChanged) {
                    ctrl.openCancelConfirmation('viewLoggedInProfileClick', ctrl.viewUserObj, ctrl.isLoggedIn);
                }
                else {
                    this.viewProfileDisplay = true;
                    this.editUserInfo = item;
                    if (!this.loggedInUserDisplay) {
                        this.getLoggedInUser();
                    }
                }
            });

        //As starts get all roles and its associated user in roleList
        this.getRoles();
        //Get featureScreenMap
        // this.getFeaturesBasedOnScreen();
        //Get Logged In User Info
        this.getLoggedInUser();
        this.changeSizeFromViewPort();
        //User loads usermanagement from main menu
        this.subscription = this.common.getUserManagementPage()
            .subscribe(item => {
                if (ctrl.form.dirty || ctrl.isProfilePictureChanged) {
                    ctrl.openCancelConfirmation('viewUserManagementPage', ctrl.viewUserObj, ctrl.isLoggedIn);
                }
                else {
                    this.editUserInfo = false;
                    this.viewProfileDisplay = false;
                }
            });
        //On changing profile picture from header - drop down , changing in view 
        //if currently logged in user is in view
        this.subscription = this.common.getProfilePicInUserManagementComponent()
            .subscribe(function (item: any) {
                if (ctrl.loggedInUserDisplay == true)
                    ctrl.profileImageUrl = ctrl.getBase64(item);
            });
    }

    ngAfterViewInit() {
        this.changeSizeFromViewPort();
    }

    changeSizeFromViewPort() {
        var panelGot = this.common.winResize('menuJ', 'paneltitleJ');
        $('.panelJ').css('height', panelGot - 20);
    }

    hasChanges() {
        return (this.form.dirty || this.isProfilePictureChanged);
    }

    // rcw:comment Shreya | - Get User Roles along with the users associated with them
    getRoles() {
        var ctrl = this;
        if (this.readAccess) {
            //  this.isRequesting = true;
            ctrl.roleList = [];
            this.userManagementService.getRolesWithUsers().then(function (roles) {
                ctrl.roleList = _.cloneDeep(roles);
            });
        } else {
            this.userManagementService.getUserByUserName(this.common.getLoggedInUser().email).then(function (obatinedUser) {
                ctrl.roleList = obatinedUser.roles;
            });
        }
    }
    // rcw:comment Shreya | - Get Logged In user information 
    // to be shown when user comes on user management page
    getLoggedInUser() {
        let ctrl = this;
        //Retrieving logged user from local storage
        this.isRequesting = true;
        this.loggedInUserById = _.cloneDeep(this.common.getLoggedInUser());
        //getting the logged in user object by id 
        this.userManagementService.getUserByUserName(this.loggedInUserById.email).then(function (obatinedUser) {
            ctrl.loggedInUser = _.cloneDeep(obatinedUser);
            //getting the logged in user object by id for view purpose in whole system 
            ctrl.viewUserObj = _.cloneDeep(obatinedUser);
            //Get Logged in user's profile picture
            ctrl.userManagementService.getUserAvatar(ctrl.loggedInUserById.email)
                .then(function (profileImageUrl) {
                    //Get Feature access for logged in user
                    ctrl.getFeaturesBasedOnScreen();
                    // If Profile Picture is there - set profileImageuRL
                    if (profileImageUrl != "" && profileImageUrl !== null) {
                        ctrl.profileImageUrl = "data:image/jpeg;base64," + profileImageUrl;
                    }
                    else {
                        ctrl.profileImageUrl = null;
                    }
                    //After fetching image - setting isRequesting false
                    ctrl.isRequesting = false;
                })
                .catch(function (error) {
                    ctrl.isRequesting = false;
                    console.log("Error ::", error)
                });
        });
        ctrl.loggedInUserDisplay = true;
    }

    // rcw:comment Shreya | - Get Features Associated with the screen and roles
    getFeaturesBasedOnScreen() {
        this.featureScreenMap = this.headerService.getMenu();
        let roleList = this.viewUserObj.roles;
        var roleIds = _.map(roleList, 'roleId');
        this.getFeatureAccessListBasedOnRolesByIds(roleIds);
    }

    // rcw:comment Shreya | - Whenver user clicks on edit details button open the edit
    //  form filling all the user information of selected user
    editUser() {
        //Clear the form and user object along with the resetting the flags
        if (_.isEmpty(this.currentFeatureAccessList) && _.isEmpty(this.currentMainMenu)) {
            this.currentFeatureAccessList = this.featureAccessList;
            this.currentMainMenu = this.mainMenu;
            this.currentMainMenu = _.cloneDeep(this.mainMenu);
        }
        this.clearObjects();
        this.editUserInfo = true;
        this.viewUserInfo = false;
        this.userObj = _.cloneDeep(this.viewUserObj);
        //Set user roles associated with the selected user
        this.setUserRoles(this.userObj);
    }

    // rcw:comment Shreya | - Remove profile picture
    removeProfilePicture() {
        if (this.currentProfilePicImageUrl == null) {
            this.currentProfilePicImageUrl = this.profileImageUrl;
        }
        if (this.uploadedFiles.length > 0) {
            this.uploadedFiles = [];
            this.fileUpload.remove(0);
            this.profileImageUrl = null;
        } else {
            this.profileImageUrl = null;
        }
        this.isProfilePictureChanged = true;
    }
    // rcw:comment Shreya | - Convert Image FILE to base64 URL
    getBase64(file) {
        let ctrl = this;
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            ctrl.profileImageUrl = reader.result;
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }
    // rcw:comment Shreya | - Set Roles of user associated into the rolelist on update
    setUserRoles(user: User) {
        if (user.roles.length > 0) {
            for (let mainRole of this.roleList) {
                mainRole.selected = false;
                for (let userRole of user.roles) {
                    if (userRole.roleId === mainRole.roleId) {
                        mainRole.selected = true;
                    }
                }
            }
            this.changeUserFeatureByRole();
        }
    }
    // rcw:comment Shreya | - Set Image file on uploading image
    onSelect(event: any) {
        this.uploadedFiles = [];
        this.isProfilePictureChanged = true;
        //checks whether the uploaded image size is valid or not
        if (event.files[0].size > 1000000) {
            this.InvalidImageDisplay = true;
        }
        else {
            //checks whether the uploaded file type is valid or not
            var file = event.files[0];
            var fileType = file["type"];
            var ValidImageTypes = ["image/gif", "image/jpeg", "image/png", "image/jpg", "icon/ico", "image/bmp"];
            if ($.inArray(fileType, ValidImageTypes) < 0) {
                this.InvalidFileDisplay = true;
            }
            else {
                this.uploadedFiles.push(event.files[0]);
            }
        }
    }

    // rcw:comment Shreya | - Check whether user has made any changes into the form.
    //  If yes then open confirmation popup else navigate to the view info page
    checkCancelChanges() {
        //This is called when clicked on edit details and the cancel changes
        if (this.userObj.userId !== null) {
            if (this.form.dirty || this.isProfilePictureChanged) {
                this.openCancelConfirmation('addoreditedUser', null, false);
            } else {
                //clicked on edit but didnt make any changes
                //So profileimageurl was unaffected so no need to restore that
                //But clear objects was called and featute-access table details got cleared
                //So restoring it.
                this.editUserInfo = false;
                this.viewUserInfo = true;
                this.userObj = new User();
                this.clearObjects();
                if (!(_.isEmpty(this.currentFeatureAccessList) && _.isEmpty(this.currentMainMenu))) {
                    this.featureAccessList = this.currentFeatureAccessList;
                    this.mainMenu = this.currentMainMenu;
                }
                this.currentFeatureAccessList = [];
                this.currentMainMenu = {};
            }
        } else {
            //This is called when clicked on add and then cancelling changes
            if (this.form.dirty || this.isProfilePictureChanged) {
                this.openCancelConfirmation('addoreditedUser', null, false);
            } else {
                //Clicked on add - but made no changes
                //So profileImageUrl become null since clicked on add - restoring it 
                //And feature-access table also got cleared because clicked on add
                //clearObject - cleared it. so restoring it also
                if (this.currentProfilePicImageUrl != null) {
                    this.profileImageUrl = this.currentProfilePicImageUrl;
                }
                this.currentProfilePicImageUrl = null;
                this.editUserInfo = false;
                this.viewUserInfo = true;
                this.userObj = new User();
                this.clearObjects()
                if (!(_.isEmpty(this.currentFeatureAccessList) && _.isEmpty(this.currentMainMenu))) {
                    this.featureAccessList = this.currentFeatureAccessList;
                    this.mainMenu = this.currentMainMenu;
                }
                this.currentFeatureAccessList = [];
                this.currentMainMenu = {};
            }
        }
    }

    // rcw:comment Shreya | - Whenever User click on the user when already user info has changed then cancel user changes
    changeUserDetails() {
        //This method will be called while editing clicking on some other user, then
        //profile picture of previously edited user was saved in currentProfilePicImageUrl
        //So emptying it. 
        this.currentProfilePicImageUrl = null;
        let ctrl = this;
        this.clearObjects();
        this.currentFeatureAccessList = [];
        this.currentMainMenu = {};
        this.viewUserInfo = true;
        this.editUserInfo = false;
        this.isProfilePictureChanged = false;
        if (this.isLoggedIn) {
            this.loggedInUserDisplay = true;
        } else {
            this.loggedInUserDisplay = false;
        }
        //Getting the user object , using modalContent.userId
        //Because modelContent didnt have roles in it
        ctrl.isRequesting = true;
        this.userManagementService.getUserByUserName(ctrl.modalContent.email).then(function (retrievedUser) {
            ctrl.viewUserObj = _.cloneDeep(retrievedUser);
            ctrl.getFeaturesBasedOnScreen();
        });
        //Set Profile Picture of user
        this.userManagementService.getUserAvatar(this.modalContent.email)
            .then(function (profileImageUrl) {
                // If Profile Picture is there
                if (profileImageUrl != "" && profileImageUrl !== null)
                    ctrl.profileImageUrl = "data:image/jpeg;base64," + profileImageUrl;
                else
                    ctrl.profileImageUrl = null;

                ctrl.isRequesting = false;
            })
            .catch(function (error) {
                console.log("Error ::", error)
                ctrl.isRequesting = false;
            });
        this.cancelUserDisplay = false;
    }

    // rcw:comment Shreya | - Cancel user changes and visit profile of logged in user
    cancelChangeViewProfile() {
        this.clearObjects();
        //while editing logged in user, clicking on view profile
        //Then currentProfilePicImageUrl stores the profile pic of user before editing
        //Since cancelling changes, setting the profile picture back to what it was
        //Now if editing some other user other than logged in user and then going on view profile
        //Then getLoggedInUser will be called than this will be ignored.
        if (this.currentProfilePicImageUrl != null) {
            this.profileImageUrl = this.currentProfilePicImageUrl;
        }
        this.currentProfilePicImageUrl = null;
        if (!(_.isEmpty(this.currentFeatureAccessList) && _.isEmpty(this.currentMainMenu))) {
            this.featureAccessList = this.currentFeatureAccessList;
            this.mainMenu = this.currentMainMenu;
        }
        this.currentFeatureAccessList = [];
        this.currentMainMenu = {};
        this.viewUserInfo = true;
        this.editUserInfo = false;
        this.isProfilePictureChanged = false;
        //If Logged in user not in display than get logged in user
        if (!this.loggedInUserDisplay) {
            this.getLoggedInUser();
        }
        this.viewProfileDisplay = true;
        this.cancelUserDisplay = false;
    }

    // rcw:comment Shreya | - Cancel user changes and visit user management page
    cancelChangeUserManagementPage() {
        //While editing some user - clicking on main menu - usermanagement
        //Then cancel changes, while set the profile picture as it was before
        if (this.currentProfilePicImageUrl != null) {
            this.profileImageUrl = this.currentProfilePicImageUrl;
        }
        this.currentProfilePicImageUrl = null;
        this.clearObjects();
        if (!(_.isEmpty(this.currentFeatureAccessList) && _.isEmpty(this.currentMainMenu))) {
            this.featureAccessList = this.currentFeatureAccessList;
            this.mainMenu = this.currentMainMenu;
        }
        this.currentFeatureAccessList = [];
        this.currentMainMenu = {};
        this.viewUserInfo = true;
        this.editUserInfo = false;
        this.isProfilePictureChanged = false;
        this.cancelUserDisplay = false;
        this.viewProfileDisplay = false;
    }

    // rcw:comment Shreya | - Cancel user changes and clear objects
    cancelChanges() {
        this.editUserInfo = false;
        this.viewUserInfo = true;
        this.userObj = new User();
        this.cancelUserDisplay = false;
        this.clearObjects();
        if (!(_.isEmpty(this.currentFeatureAccessList) && _.isEmpty(this.currentMainMenu))) {
            this.featureAccessList = this.currentFeatureAccessList;
            this.mainMenu = this.currentMainMenu;
        }
        this.currentFeatureAccessList = [];
        this.currentMainMenu = {};
        //Profile picture set as it was before editing
        if (this.currentProfilePicImageUrl != null) {
            this.profileImageUrl = this.currentProfilePicImageUrl;
        }
        this.currentProfilePicImageUrl = null;
    }

    // rcw:comment Shreya | - Open add user page clearing user objects and form dirtyness
    openAddUserPage() {
        //While editing some user, clicking on add new user,
        //then currentProfilePicImageUrl contains the profile picture of previous user
        //So emptying it
        if (this.currentProfilePicImageUrl == null) {
            this.currentProfilePicImageUrl = this.profileImageUrl;
        }
        if (_.isEmpty(this.currentFeatureAccessList) && _.isEmpty(this.currentMainMenu)) {
            this.currentFeatureAccessList = this.featureAccessList;
            this.currentMainMenu = this.mainMenu;
        }
        if (this.form.dirty || this.isProfilePictureChanged) {
            this.openCancelConfirmation('addUserClick', null, false);
        } else {

            this.editUserInfo = true;
            this.viewUserInfo = false;
            this.userObj = new User();
            this.profileImageUrl = null;
            this.loggedInUserDisplay = false;
            this.clearObjects();
        }
    }

    // rcw:comment Shreya | - Navigate to new user when user cancel's new changes
    navigateToNewUser() {
        this.loggedInUserDisplay = false;
        this.editUserInfo = true;
        this.viewUserInfo = false;
        this.cancelUserDisplay = false;
        this.userObj = new User();
        this.profileImageUrl = null;
        this.clearObjects();
    }

    // rcw:comment Shreya | - Add/Update user after validating the user and its roles
    onSubmit() {
        this.submittedFlag = true;
        if (_.some(this.roleList, ['selected', true])) {
            this.roleError = false;
            if (this.form.valid) {
                this.submittedFlag = false;
                this.userObj.roles = [];
                for (let role of this.roleList) {
                    if (role.selected) {
                        let tempRole: Role = _.cloneDeep(role);
                        this.userObj.roles.push(tempRole);
                    }
                }
                this.userObj.userProfile = this.uploadedFiles[0];
                this.addOrUpdateUsers();
            } else {
                window.scrollTo(0, 0);
            }
        } else {
            this.roleError = true;
            window.scrollTo(0, 0);
        }
    }

    addOrUpdateUsers() {
        let ctrl = this;
        if (this.userObj.userId === null) {
            this.isRequesting = true;
            let tempProfilePictureFile = ctrl.userObj.userProfile;
            this.userManagementService.add(this.userObj).then(function (successObj) {
                //While editing or adding new user, clearing currentProfilePicImageUrl
                //Since we are successfully adding/editing so canel and setting back its profile picture 
                //scenarios will not occur
                ctrl.currentProfilePicImageUrl = null;
                ctrl.currentFeatureAccessList = [];
                ctrl.currentMainMenu = {};
                ctrl.userObj = _.cloneDeep(successObj.details);
                //If Uploaded Profile Picture, Add profile picture 
                if (tempProfilePictureFile != null && tempProfilePictureFile != undefined) {
                    ctrl.userManagementService.uploadUserAvatar(tempProfilePictureFile, ctrl.userObj.email)
                        .then(function () {
                            ctrl.profileImageUrl = ctrl.getBase64(tempProfilePictureFile);
                            if (successObj.successDescription != undefined && successObj.successDescription != null && successObj.successDescription != "") {
                                ctrl.toasterService.sendToaster('success', 'User', successObj.successDescription)
                            } else {
                                ctrl.toasterService.sendToaster('success', 'User', 'User created successfully')
                            }
                        }).then(function () {
                            ctrl.getRoles();
                            ctrl.clearObjects();
                        }).then(function () {
                            ctrl.showUserDetail(ctrl.userObj, ctrl.loggedInUserDisplay, false);
                        })
                        .catch(function (error) {
                            ctrl.isRequesting = false;
                            if (error && error._body) {
                                ctrl.toasterService.sendToaster('error', 'User', JSON.parse(error._body).error_description)
                            } else {
                                ctrl.toasterService.sendToaster('error', 'User', 'Profile Pic failed')
                            }
                        });
                } else {
                    if (successObj.successDescription != undefined && successObj.successDescription != null && successObj.successDescription != "") {
                        ctrl.toasterService.sendToaster('success', 'User', successObj.successDescription)
                    } else {
                        ctrl.toasterService.sendToaster('success', 'User', 'User created successfully')
                    }
                }
            }).then(function () {
                if (!(tempProfilePictureFile != null && tempProfilePictureFile != undefined)) {
                    ctrl.getRoles();
                    ctrl.clearObjects();
                }
            }).then(function () {
                if (!(tempProfilePictureFile != null && tempProfilePictureFile != undefined)) {
                    ctrl.showUserDetail(ctrl.userObj, ctrl.loggedInUserDisplay, false);
                }
            }).catch(function (error) {
                if (error && error._body) {
                    ctrl.toasterService.sendToaster('error', 'User', JSON.parse(error._body).error_description)
                } else {
                    ctrl.toasterService.sendToaster('error', 'User', 'User creation failed')
                }
                ctrl.isRequesting = false;
            });
        } else {
            this.isRequesting = true;
            let tempProfilePictureFile = ctrl.userObj.userProfile;
            this.userObj.screenPermissions = [];
            this.userManagementService.update(this.userObj).then(function (successObj) {
                //While editing or adding new user, clearing currentProfilePicImageUrl
                //Since we are successfully adding/editing so canel and setting back its profile picture 
                //scenarios will not occur
                ctrl.currentProfilePicImageUrl = null;
                ctrl.currentFeatureAccessList = [];
                ctrl.currentMainMenu = {};
                ctrl.userObj = _.cloneDeep(successObj.details);
                if (ctrl.userObj.userId == ctrl.loggedInUser.userId) {
                    ctrl.loggedInUser = _.cloneDeep(ctrl.userObj);
                }
                //Delete Profile Picture if profile picture removed
                if (ctrl.isProfilePictureChanged == true && ctrl.profileImageUrl == null && tempProfilePictureFile == null) {
                    ctrl.userManagementService.deleteUserAvatar(ctrl.userObj.email).then(function () {
                        if (successObj.successDescription != undefined && successObj.successDescription != null && successObj.successDescription != "") {
                            ctrl.toasterService.sendToaster('success', 'User', successObj.successDescription)
                        } else {
                            ctrl.toasterService.sendToaster('success', 'User', 'User updated successfully')
                        }
                    }).then(function () {
                        ctrl.getRoles();
                        ctrl.clearObjects();
                    }).then(function () {
                        ctrl.showUserDetail(ctrl.userObj, ctrl.loggedInUserDisplay, false);
                    }).catch(function (error) {
                        ctrl.isRequesting = false;
                        if (error && error._body) {
                            ctrl.toasterService.sendToaster('error', 'User', JSON.parse(error._body).error_description)
                        } else {
                            ctrl.toasterService.sendToaster('error', 'User', 'Profile Pic Deletion failed')
                        }
                        console.log("Error is", error);
                    });
                }
                //If Uploaded Profile Picture, Add profile picture 
                else if (ctrl.isProfilePictureChanged == true && tempProfilePictureFile != null && tempProfilePictureFile != undefined) {
                    ctrl.userManagementService.uploadUserAvatar(tempProfilePictureFile, ctrl.userObj.email)
                        .then(function () {
                            ctrl.profileImageUrl = ctrl.getBase64(tempProfilePictureFile);
                            if (successObj.successDescription != undefined && successObj.successDescription != null && successObj.successDescription != "") {
                                ctrl.toasterService.sendToaster('success', 'User', successObj.successDescription)
                            } else {
                                ctrl.toasterService.sendToaster('success', 'User', 'User updated successfully')
                            }
                        }).then(function () {
                            ctrl.getRoles();
                            ctrl.clearObjects();
                        }).then(function () {
                            ctrl.showUserDetail(ctrl.userObj, ctrl.loggedInUserDisplay, false);
                        })
                        .catch(function (error) {
                            ctrl.isRequesting = false;
                            if (error && error._body) {
                                ctrl.toasterService.sendToaster('error', 'User', JSON.parse(error._body).error_description)
                            } else {
                                ctrl.toasterService.sendToaster('error', 'User', 'Profile Pic Updation failed')
                            }
                        });
                } else if (!ctrl.isProfilePictureChanged) {
                    if (successObj.successDescription != undefined && successObj.successDescription != null && successObj.successDescription != "") {
                        ctrl.toasterService.sendToaster('success', 'User', successObj.successDescription)
                    } else {
                        ctrl.toasterService.sendToaster('success', 'User', 'User updated successfully')
                    }
                }
            }).then(function () {
                if (!ctrl.isProfilePictureChanged) {
                    ctrl.getRoles();
                    ctrl.clearObjects();
                    ctrl.isRequesting = false;
                }
            }).then(function () {
                if (!ctrl.isProfilePictureChanged) {
                    ctrl.showUserDetail(ctrl.userObj, ctrl.loggedInUserDisplay, false);
                }
            }).catch(function (error) {
                ctrl.isRequesting = false;
                if (error && error._body) {
                    ctrl.toasterService.sendToaster('error', 'User', JSON.parse(error._body).error_description)
                } else {
                    ctrl.toasterService.sendToaster('error', 'User', 'User Updation failed')
                }
            });
        }
    }

    clearObjects() {
        this.roleError = false;
        this.rolesSelected = false;
        this.submittedFlag = false;
        for (let item of this.roleList) {
            item.selected = false;
        }
        this.isProfilePictureChanged = false;
        this.fileUpload.remove(0);
        this.uploadedFiles = [];
        this.mainMenu = {};
        this.featureAccessList = [];
        //  this.getFeaturesBasedOnScreen();
        window.scrollTo(0, 0);
        Object.keys(this.form.form.controls).forEach(control => {
            this.form.form.controls[control].markAsPristine();
        });
    }


    // rcw:comment Shreya | - Get Feature Access based on all roles 
    getFeatureAccessListBasedOnRolesByIds(roleIds: number[]) {
        let ctrl = this;
        ctrl.mainMenu = {};
        ctrl.featureAccessList = [];
        if (roleIds != null && roleIds != undefined && !(_.isEmpty(roleIds))) {
            // this.isRequesting = true;
            if (this.isRequesting === false) {
                this.isRequesting = true;
            }
            this.userManagementService.getScreenAccessFromRoleId(roleIds).then(function (featureAccessList) {
                ctrl.mainMenu = {};
                ctrl.featureAccessList = [];
                for (let item of featureAccessList) {
                    if (!_.includes(ctrl.featureAccessList, item['screenGroup'])) {
                        ctrl.featureAccessList.push(item['screenGroup']);
                    }
                    if (ctrl.mainMenu[item['screenGroup']] != undefined) {
                        ctrl.mainMenu[item['screenGroup']].push(item);
                    } else {
                        ctrl.mainMenu[item['screenGroup']] = [];
                        ctrl.mainMenu[item['screenGroup']].push(item);
                    }
                }
                ctrl.isRequesting = false;
            }).catch(function (error) {
                ctrl.isRequesting = false;
                console.log("Error is", error);
            });
        }
        else {
            ctrl.mainMenu = null;
        }
    }

    changeUserFeatureByRole() {
        let roleIds;
        if (_.some(this.roleList, ['selected', true])) {
            this.rolesSelected = true;
            let ctrl = this;
            let roleSelected = _.filter(this.roleList, function (role) { return role.selected == true; });
            if (roleSelected != undefined) {
                roleIds = _.map(roleSelected, 'roleId');
            }
        } else {
            this.rolesSelected = false;
        }
        //Based on the roles selected, getting the ids of those roles and fetching the feature acc    ess
        this.getFeatureAccessListBasedOnRolesByIds(roleIds);

    }

    openRolesDrop(value) {
        if (value != undefined && value != null && value != "") {
            for (let i = 0; i < this.roleList.length; i++) {
                $("#roles" + i).next('ul').show();
                //Get the first child of the ul and change the caret symbol from open to close and vice versa
                $("#roles" + i + " i:first-child").addClass("open-caret");
                $("#roles" + i + " i:first-child").removeClass("close-caret");
            }
        } else {
            for (let i = 0; i < this.roleList.length; i++) {
                $("#roles" + i).next('ul').hide();
                //Get the first child of the ul and change the caret symbol from open to close and vice versa
                $("#roles" + i + " i:first-child").addClass("close-caret");
                $("#roles" + i + " i:first-child").removeClass("open-caret");
            }
        }
    }

    changeState(id, e) {
        //On click of ul toggle the open/close dropdown
        $("#" + id).next('ul').toggle();
        if ($("#" + id).next('ul').is(':visible')) {
            //Get the first child of the ul and change the caret symbol from open to close and vice versa
            $("#" + id + " i:first-child").addClass("open-caret");
            $("#" + id + " i:first-child").removeClass("close-caret");
        } else {
            $("#" + id + " i:first-child").addClass("close-caret");
            $("#" + id + " i:first-child").removeClass("open-caret");
        }
    }

    showUserDetail(user: User, isLoggedIn, callUser) {
        if (this.form.dirty || this.isProfilePictureChanged) {
            this.openCancelConfirmation('changeUser', user, isLoggedIn);
        } else {
            if (callUser) {
                let ctrl = this;
                this.isRequesting = true;
                this.userManagementService.getUserByUserName(user.email).then(function (retrievedUser) {
                    if (retrievedUser != null) {
                        //On successful retreival of User , get User's Profile Picture
                        ctrl.userManagementService.getUserAvatar(user.email)
                            .then(function (profileImageUrl) {
                                if (profileImageUrl != "" && profileImageUrl !== null) {
                                    ctrl.profileImageUrl = "data:image/jpeg;base64," + profileImageUrl;
                                }
                                else {
                                    ctrl.profileImageUrl = null;
                                }
                                ctrl.viewUserInfo = true;
                                ctrl.editUserInfo = false;
                                ctrl.roleError = false;
                                ctrl.rolesSelected = false;
                                if (isLoggedIn) {
                                    ctrl.loggedInUserDisplay = true;
                                } else {
                                    if (ctrl.loggedInUser.userId == user.userId) {
                                        ctrl.loggedInUserDisplay = true;
                                    } else {
                                        ctrl.loggedInUserDisplay = false;
                                    }
                                }
                                ctrl.viewUserObj = _.cloneDeep(retrievedUser);
                                ctrl.getFeaturesBasedOnScreen();
                            })
                            .catch(function (error) {
                                console.log("Error ::", error)
                                ctrl.isRequesting = false;
                            });
                    }
                }).catch(function (error) {
                    if (error && error._body) {
                        ctrl.toasterService.sendToaster('error', 'User', JSON.parse(error._body).error_description)
                    } else {
                        ctrl.toasterService.sendToaster('error', 'User', 'User retrieval failed');
                        ctrl.modalContent = new User();
                    }
                    ctrl.isRequesting = false;
                });
            } else {
                this.viewUserInfo = true;
                this.editUserInfo = false;
                this.roleError = false;
                this.rolesSelected = false;
                if (isLoggedIn) {
                    this.loggedInUserDisplay = true;
                } else {
                    this.loggedInUserDisplay = false;
                }
                this.loggedInUserRoles = '';
                this.viewUserObj = _.cloneDeep(user);
                this.getFeaturesBasedOnScreen();

            }
        }
    }

    openCancelConfirmation(cancelChangeReason, user, isLoggedIn) {
        this.cancelChangeReason = cancelChangeReason;
        this.cancelUserDisplay = true;
        if (user != undefined && user != null) {
            this.modalContent = _.cloneDeep(user);
            this.isLoggedIn = isLoggedIn
        }
    }
    invalidImageDisplay() {
        this.InvalidImageDisplay = false;
        this.InvalidFileDisplay = false;
    }
    // rcw:comment Shreya |  - Open Delete Modal Confirmation Popup
    openDeleteConfirmation(user) {
        this.modalContent = _.cloneDeep(user);
        this.deleteUserDisplay = true;
    }

    // rcw:comment Shreya |  - Delete User by selected Id
    deleteUser() {
        var ctrl = this;
        this.isRequesting = true;
        this.userManagementService.delete(ctrl.modalContent.userId).then(function (successObj) {
            if (successObj.successDescription != undefined && successObj.successDescription != null && successObj.successDescription != "") {
                ctrl.toasterService.sendToaster('success', 'User', successObj.successDescription)
            } else {
                ctrl.toasterService.sendToaster('success', 'User', 'User Deleted successfully')
            }
            ctrl.getRoles();
        }).then(function () {
            //If deleting user - while viewing or editing that user
            if (ctrl.modalContent.userId == ctrl.viewUserObj.userId) {
                ctrl.clearObjects();
                ctrl.isRequesting = true;
                ctrl.viewUserInfo = true;
                ctrl.editUserInfo = false;
                ctrl.getLoggedInUser();
                ctrl.isRequesting = false;
            }
            ctrl.modalContent = new User();
            ctrl.isRequesting = false;
        }).catch(function (error) {
            if (error && error._body) {
                ctrl.toasterService.sendToaster('error', 'User', JSON.parse(error._body).error_description)
            } else {
                ctrl.toasterService.sendToaster('error', 'User', 'User deletion failed');
                ctrl.modalContent = new User();
            }
            ctrl.isRequesting = false;
        });
        this.deleteUserDisplay = false;
    }

    // rcw:comment Shreya |  - Generates the dynamic context menu for the right click on the user name
    //  for the various actions like Delete
    public onContextMenu($event: MouseEvent, item: any): void {
        this.configMenuOptions = [];
        this.configMenuOptions.push({
            html: () => 'Delete',
            click: (item) => {
                //Open delete confirmation message to notify user
                this.openDeleteConfirmation(item);
            },
            enabled: (item: User): boolean => {
                if (this.updateAccess) {
                    if (this.loggedInUser.userId == item.userId) {
                        return false;
                    }
                }
                return this.updateAccess;
            }
        })
        this.contextMenuService.show.next({
            actions: this.configMenuOptions,
            event: $event,
            item: item
        });
        $event.preventDefault();
    }
}
