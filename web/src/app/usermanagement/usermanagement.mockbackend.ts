/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import {BaseRequestOptions, Response, ResponseOptions, RequestMethod, Http} from '@angular/http';
import {MockBackend, MockConnection} from '@angular/http/testing';
import {UserManagementMockDataService} from '../usermanagement/usermanagement.mockdata.service';
import {HttpService} from '../services/http.service';

export let UserManagementMockBackendProvider = {
    provide: HttpService,
    useFactory: (backend: MockBackend, options: BaseRequestOptions) => {
        // configure fake backend
        backend.connections.subscribe((connection: MockConnection) => {
            let userManagementMockDataService = new UserManagementMockDataService();

            // rcw:comment Shreya | Get All Users
            if (connection.request.url.endsWith('/usermanagement/users') && connection.request.method === RequestMethod.Get) {
                let userList = userManagementMockDataService.getUsers();
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: userList})
                ));
            }
            // rcw:comment Shreya | Get All Roles with users
            if (connection.request.url.endsWith('/usermanagement/roles/users') && connection.request.method === RequestMethod.Get) {
                let userList = userManagementMockDataService.getRolesWithUsers();
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: userList})
                ));
            }
            var regexp = new RegExp('\\/usermanagement\\/users\\/[0-9]+');
            // rcw:comment Shreya | Get User By Id
            if (regexp.test(connection.request.url) && connection.request.method === RequestMethod.Get) {
                var mockId = connection.request.url.match(regexp)[0];
                var urlStrngs = mockId.split('/');
                let user = userManagementMockDataService.getUser(parseInt(urlStrngs[urlStrngs.length - 1]));
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: user})
                ));
            }
            // rcw:comment Shreya | Add User
            if (connection.request.url.endsWith('/usermanagement/users') && connection.request.method === RequestMethod.Post) {
                let user = JSON.parse(connection.request.getBody());
                user.userId = 8;
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: user})
                ));
            }
            // rcw:comment Shreya | Update User
            if (connection.request.url.endsWith('/usermanagement/users') && connection.request.method === RequestMethod.Put) {
                let user = JSON.parse(connection.request.getBody());
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: user})
                ));
            }
        });
        return new HttpService(backend, options);
    },
    deps: [MockBackend, BaseRequestOptions]
};
