/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import {User} from './user';
import {Role} from './role';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs';

export class UserManagementMockDataService {

    constructor() {}

    getUsers(): User[] {

        let role1 = new Role();
        role1.roleId = 1;
        role1.roleName = 'Executive';

        let role2 = new Role();
        role2.roleId = 2;
        role2.roleName = 'CVM';

        let role3 = new Role();
        role3.roleId = 3;
        role3.roleName = 'Campaign Analyst';

        let userList: User[] = [];
        let user = new User();
        user.userId = 1;
        user.firstName = "Julius";
        user.lastName = "Fernandes";
        user.roles.push(role1);
        user.screenPermissions = [
            {
                "screenGroup": "EXPLORATION",
                "screenName": "BDNA_EXPLORATION",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "CAMPAIGN_ANALYSIS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_AND_OFFER_ANALYSIS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_AND_OFFER_ANALYSIS_SIM",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_EXPLORATION",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_EXPLORATION_SIM",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "OPERATIONS",
                "screenName": "MONITORING",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "REPORTS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "ADMIN",
                "screenName": "USER_MANAGEMENT",
                "accessLevel": "UPDATE_ACCESS"
            }
        ];
        userList.push(user);
        user = new User();
        user.userId = 2;
        user.firstName = "Subbu";
        user.lastName = "Bharvani";
        user.roles.push(role2);
        user.roles.push(role1);
        user.screenPermissions = [
            {
                "screenGroup": "EXPLORATION",
                "screenName": "BDNA_EXPLORATION",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "CAMPAIGN_ANALYSIS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_AND_OFFER_ANALYSIS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_AND_OFFER_ANALYSIS_SIM",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_EXPLORATION",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_EXPLORATION_SIM",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "OPERATIONS",
                "screenName": "MONITORING",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "REPORTS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "ADMIN",
                "screenName": "USER_MANAGEMENT",
                "accessLevel": "UPDATE_ACCESS"
            }
        ];
        userList.push(user);
        user = new User();
        user.userId = 3;
        user.firstName = "Fede";
        user.lastName = "Muller";
        user.roles.push(role3);
        user.roles.push(role1);
        user.screenPermissions = [
            {
                "screenGroup": "EXPLORATION",
                "screenName": "BDNA_EXPLORATION",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "CAMPAIGN_ANALYSIS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_AND_OFFER_ANALYSIS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_AND_OFFER_ANALYSIS_SIM",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_EXPLORATION",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_EXPLORATION_SIM",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "OPERATIONS",
                "screenName": "MONITORING",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "REPORTS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "ADMIN",
                "screenName": "USER_MANAGEMENT",
                "accessLevel": "UPDATE_ACCESS"
            }
        ];
        userList.push(user);
        user = new User();
        user.userId = 4;
        user.firstName = "Balakrishnan";
        user.lastName = "Raman";
        user.roles.push(role3);
        user.screenPermissions = [
            {
                "screenGroup": "EXPLORATION",
                "screenName": "BDNA_EXPLORATION",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "CAMPAIGN_ANALYSIS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_AND_OFFER_ANALYSIS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_AND_OFFER_ANALYSIS_SIM",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_EXPLORATION",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_EXPLORATION_SIM",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "OPERATIONS",
                "screenName": "MONITORING",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "REPORTS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "ADMIN",
                "screenName": "USER_MANAGEMENT",
                "accessLevel": "UPDATE_ACCESS"
            }
        ];
        userList.push(user);
        user = new User();
        user.userId = 5;
        user.firstName = "Thomas";
        user.lastName = "Bottrill";
        user.roles.push(role3);
        user.screenPermissions = [
            {
                "screenGroup": "EXPLORATION",
                "screenName": "BDNA_EXPLORATION",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "CAMPAIGN_ANALYSIS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_AND_OFFER_ANALYSIS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_AND_OFFER_ANALYSIS_SIM",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_EXPLORATION",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_EXPLORATION_SIM",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "OPERATIONS",
                "screenName": "MONITORING",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "REPORTS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "ADMIN",
                "screenName": "USER_MANAGEMENT",
                "accessLevel": "UPDATE_ACCESS"
            }
        ];
        userList.push(user);
        user = new User();
        user.userId = 6;
        user.firstName = "Greg";
        user.lastName = "Armstrong";
        user.roles.push(role3);
        user.screenPermissions = [
            {
                "screenGroup": "EXPLORATION",
                "screenName": "BDNA_EXPLORATION",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "CAMPAIGN_ANALYSIS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_AND_OFFER_ANALYSIS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_AND_OFFER_ANALYSIS_SIM",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_EXPLORATION",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_EXPLORATION_SIM",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "OPERATIONS",
                "screenName": "MONITORING",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "REPORTS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "ADMIN",
                "screenName": "USER_MANAGEMENT",
                "accessLevel": "UPDATE_ACCESS"
            }
        ];
        userList.push(user);
        user = new User();
        user.userId = 7;
        user.firstName = "Manav";
        user.lastName = "Khanna";
        user.roles.push(role3);
        user.screenPermissions = [
            {
                "screenGroup": "EXPLORATION",
                "screenName": "BDNA_EXPLORATION",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "CAMPAIGN_ANALYSIS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_AND_OFFER_ANALYSIS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_AND_OFFER_ANALYSIS_SIM",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_EXPLORATION",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "DROP_OFF_EXPLORATION_SIM",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "OPERATIONS",
                "screenName": "MONITORING",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "EXPLORATION",
                "screenName": "REPORTS",
                "accessLevel": "UPDATE_ACCESS"
            },
            {
                "screenGroup": "ADMIN",
                "screenName": "USER_MANAGEMENT",
                "accessLevel": "UPDATE_ACCESS"
            }
        ];
        userList.push(user);

        return userList;
    }

    getRolesWithUsers(): Role[] {
        let roleList: Role[] = [];
        let userList = this.getUsers();

        let role = new Role();
        role.roleId = 1;
        role.roleName = "Executive";
        role.users = [userList[0], userList[1], userList[2]];
        roleList.push(role);
        //                    
        role = new Role();
        role.roleId = 2;
        role.roleName = "CVM";
        role.users = [userList[1]];
        roleList.push(role);

        role = new Role();
        role.roleId = 3;
        role.roleName = "Campaign Analyst";
        role.users = [userList[2], userList[3], userList[4]];
        roleList.push(role);

        role = new Role();
        role.roleId = 4;
        role.roleName = "Operations Analyst";
        roleList.push(role);

        role = new Role();
        role.roleId = 5;
        role.roleName = "Admin";
        roleList.push(role);
        return roleList;
    }

    getUser(id: number): User {
        let userList = this.getUsers()
        for (let item of userList) {
            if (id == item.userId) {
                return item;
            }
        }
    }

    createNewUser(): User {
        let user = new User();
        user.firstName = "New";
        user.lastName = "User";
        user.email = "newuser@gmail.com";
        return user;
    }

}
